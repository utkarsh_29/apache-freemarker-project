# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This repository contains code for generating a java source file using Apache FreeMarker Template. After that we compile this java source file to generate a .class file and then add it to a JAR file. The last step is the load this JAR file dynamically and load the classes inside it and call its method.

### How do I get set up? ###

* CONFIGURATION: Default PORT is 8081, change the paths according to OS (currently linux based).
* DEPENDENCIES: org.freemarker, org.json, com.alibaba.fastjson, org.apache.commons
* END POINTS: 
  * Generate Java File : http://localhost:8081/eventMarker
  * Compile File and Create Jar : http://localhost:8081/eventCompile
  * Load JAR : http://localhost:8081/eventJar

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact