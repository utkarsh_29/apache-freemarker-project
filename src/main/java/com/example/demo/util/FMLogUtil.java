package com.example.demo.util;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FMLogUtil {

    private static final Logger ERROR_LOGGER = LoggerFactory.getLogger("ERROR-LOGGER");

    public FMLogUtil() {
    }

    public static FMLog getLog(String className, String functionName) {
        FMLog fmLog = new FMLog(className, functionName);
        return fmLog;
    }

    public static class FMLog {
        StringBuilder sb = new StringBuilder();
        String className;
        String functionName;

        public FMLog(String className, String functionName) {
            this.className = className;
            this.functionName = functionName;
        }

        public FMLogUtil.FMLog addKeyValue(String key, Object value) {
            this.sb.append("," + key + "[" + value + "]");
            return this;
        }

        public FMLogUtil.FMLog addCostTime(long cost) {
            this.sb.append(",cost[" + cost + "]ms");
            return this;
        }

        public FMLogUtil.FMLog addException(Exception e) {
            this.sb.append(",exceptionStacktrace[" + ExceptionUtils.getStackTrace(e) + "]");
            return this;
        }

        public FMLogUtil.FMLog addDesc(String desc) {
            this.sb.append(",desc[" + desc + "]");
            return this;
        }

        public FMLogUtil.FMLog addString(String str) {
            this.sb.append(str);
            return this;
        }

        public String toString() {
            StringBuilder builder = new StringBuilder();
            builder.append(this.className).append(".").append(this.functionName);
            builder.append(this.sb.toString());
            return builder.toString();
        }

        public void logInfo(Logger logger) {
            logger.info(getLogString(new Object[]{this.toString()}));
        }

        public void logWarn(Logger logger) {
            logger.warn(getLogString(new Object[]{this.toString()}));
        }

        public void logError(Logger logger) {
            logger.error(getLogString(new Object[]{this.toString()}));
            ERROR_LOGGER.error(getLogString(new Object[]{this.toString()}));
        }

        public void logDebug(Logger logger) {
            logger.debug(getLogString(new Object[]{this.toString()}));
        }

    }

    public static String getLogString(Object... objs) {
        StringBuilder log = new StringBuilder();
        if (objs != null) {
            Object[] arr$ = objs;
            int len$ = objs.length;

            for (int i$ = 0; i$ < len$; ++i$) {
                Object o = arr$[i$];
                log.append(o);
            }
        }

        return log.toString();
    }
}
