package com.example.demo.entity;


import java.util.List;

public class Attribute {

    List<String> imports;
    String scriptName;
    String scriptBody;
    String propertyName;

    public List<String> getImports() {
        return imports;
    }

    public void setImports(List<String> imports) {
        this.imports = imports;
    }

    public String getScriptName() {
        return scriptName;
    }

    public void setScriptName(String scriptName) {
        this.scriptName = scriptName;
    }

    public String getScriptBody() {
        return scriptBody;
    }

    public void setScriptBody(String scriptBody) {
        this.scriptBody = scriptBody;
    }

    public String getPropertyName() {
        return propertyName;
    }

    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    @Override
    public String toString() {
        return "Attribute{" +
                "imports=" + imports +
                ", scriptName='" + scriptName + '\'' +
                ", scriptBody='" + scriptBody + '\'' +
                ", propertyName='" + propertyName + '\'' +
                '}';
    }
}
