package com.example.demo.entity;

import java.util.HashMap;
import java.util.Map;

public class EventModuleContext {

    Map<String, String> extendDataMap;
    Map<String, String> context = new HashMap<>();

    public Map<String, String> getExtendDataMap() {
        return extendDataMap;
    }

    public Map<String, String> getContext() {
        return context;
    }

    public void setContext(Map<String, String> context) {
        this.context = context;
    }

    public void setExtendDataMap(Map<String, String> extendDataMap) {
        this.extendDataMap = extendDataMap;
    }

    public void addStdProperties(String propertyName, String scriptBody) {
        context.put(propertyName, scriptBody);
    }
}
