package com.example.demo.controllers;

import com.example.demo.entity.EventModuleContext;
import com.example.demo.util.FMLogUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

@RestController
@RequestMapping("/")
public class EventFileLoaderController {

    private final Logger LOG = LoggerFactory.getLogger(EventFileLoaderController.class);

    final static String pathToJar = "/home/utkarshagrawal/Downloads/MyJar.jar";  // location where jar is present

    @GetMapping("eventJar")
    public void load(String args[]) throws Exception {
        try {
            EventModuleContext eventModuleContext = new EventModuleContext();
            Map<String, String> extendDataMap = new HashMap();

            extendDataMap.put("First Name", "John");
            extendDataMap.put("Last Name", "Doe");
            extendDataMap.put("City", "Gwalior");
            eventModuleContext.setExtendDataMap(extendDataMap);

            JarFile jarFile = new JarFile(pathToJar);
            Enumeration<JarEntry> e = jarFile.entries();

            // load the txt file from the jar
            InputStream inputStream = jarFile.getInputStream(jarFile.getEntry("test/properties"));
            BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));

            for (String line; (line = in.readLine()) != null;) {
                System.out.println("Reading txt file " + line);
            }

            URL[] urls = { new URL("jar:file:" + pathToJar+"!/") };
            URLClassLoader cl = URLClassLoader.newInstance(urls);

            while (e.hasMoreElements()) {
                JarEntry je = e.nextElement();

                System.out.println(je.getName());

                if(je.isDirectory() || !je.getName().endsWith(".class")){
                    continue;
                }

                // -6 because of .class
                String className = je.getName().substring(0,je.getName().length()-6);
                className = className.replace('/', '.');

                if(!className.equals("test.EventInstance")) {
                    continue;
                }

                Class c = cl.loadClass(className);
                Object instance = c.newInstance();

                System.out.println(c.getMethods()[0].getName()+ " " + c.getMethods()[1].getName());

                c.getMethod("isCall", EventModuleContext.class).invoke(instance, eventModuleContext);
                c.getMethod("preExecute", EventModuleContext.class).invoke(instance, eventModuleContext);
                c.getMethod("postExecute", EventModuleContext.class).invoke(instance, eventModuleContext);
            }
        } catch(Exception e) {
            FMLogUtil.getLog("EventFileLoaderController", "load")
                    .addDesc("Exception while loading jar ")
                    .addException(e)
                    .logError(LOG);
            throw e;
        }
    }
}
