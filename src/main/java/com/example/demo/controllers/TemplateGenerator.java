package com.example.demo.controllers;

import com.example.demo.entity.Attribute;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TemplateGenerator {

    final static String pathToTemplate = "src/main/resources/templates/";
    final static String templateName = "EventInstance.ftl";
    final static String pathToOutputFile = "/home/utkarshagrawal/Downloads/test/";

    String instanceName;
    String outputFile;
    String conditions;

    public void generateTemplateForInstance(RiskInstanceBaseResponseDTO riskInstanceBaseResponseDTO) throws TemplateException, IOException {

        String preScriptBody = riskInstanceBaseResponseDTO.getPrePropertyScript();
        String postScriptBody = riskInstanceBaseResponseDTO.getPostPropertyScript();
        List<Attribute> attributeList = new ArrayList<>();

        if(!preScriptBody.isBlank()) {
            String[] prePropertyScriptArray = preScriptBody.split(",");
            generateAttributeList(prePropertyScriptArray, attributeList);
        }

        if(!postScriptBody.isBlank()) {
            String[] postPropertyScriptArray = postScriptBody.split(",");
            generateAttributeList(postPropertyScriptArray, attributeList);
        }

        instanceName = riskInstanceBaseResponseDTO.getInstanceName();
        outputFile = instanceName + ".java";
        conditions = riskInstanceBaseResponseDTO.getTriggerCondition();

        generateTemplate(attributeList);

    }

    private List<Attribute> generateAttributeList(String[] propertyScriptArray, List<Attribute> attributeList) {

        for(int i = 0; i < propertyScriptArray.length; i++) {
            Attribute attribute = new Attribute();

            String[] prePropertyScriptString = propertyScriptArray[i].split("|");
            attribute.setPropertyName(prePropertyScriptString[0]);
            attribute.setScriptBody(dao.fetchRiskScript(prePropertyScriptString[1]).getScriptValue());
            attribute.setScriptName(dao.fetchRiskScript(prePropertyScriptString[1]).getScriptName());

            attributeList.add(attribute);
        }

        return attributeList;
    }

    private void generateTemplate(List<Attribute> attributeList) throws IOException, TemplateException {

        /* ------------------------------------------------------------------------ */
        /* You should do this ONLY ONCE in the whole application life-cycle:        */

        /* Create and adjust the configuration singleton */
        Configuration cfg = new Configuration(Configuration.VERSION_2_3_29);
        cfg.setDirectoryForTemplateLoading(new File(pathToTemplate));
        // Recommended settings for new projects:
        cfg.setDefaultEncoding("UTF-8");
        cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
        cfg.setLogTemplateExceptions(false);
        cfg.setWrapUncheckedExceptions(true);
        cfg.setFallbackOnNullLoopVariable(false);

        /* Create a data-model */
        Map root = new HashMap();
        root.put("instanceName", instanceName);
        root.put("conditions", conditions);
        root.put("attributes", attributeList);

        /* Get the template (uses cache internally) */
        Template temp = cfg.getTemplate(templateName);

        /* Merge data-model with template */
        Writer writer = new FileWriter(new File(pathToOutputFile, outputFile));
        temp.process(root, writer);
        writer.flush();
        writer.close();

        System.out.println("Generated " + outputFile);

    }
}
