package com.example.demo.controllers;

import com.example.demo.util.FMLogUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.tools.JavaCompiler;
import javax.tools.ToolProvider;
import java.io.File;

@RestController
@RequestMapping("/")
public class EventCompileController {

    private final Logger LOG = LoggerFactory.getLogger(EventCompileController.class);

    final static String pathToJavaFile = "/home/utkarshagrawal/Downloads/test/EventInstance.java"; // here java files are present
    final static String jarName = "MyJar.jar"; // name with which jar would be created
    final static String pathToDirectory = "/home/utkarshagrawal/Downloads"; // location where jar would be created
    final static String folderName = "test"; // folder containing .class files

    @GetMapping("eventCompile")
    public void compile() throws Exception {
        try {
            JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
            ProcessBuilder processBuilder = new ProcessBuilder();

            // compile java file
            compiler.run(null, null, null, pathToJavaFile);

            // create jar file
            processBuilder.directory(new File(pathToDirectory)); // cd /home/utkarshagrawal/Downloads
            processBuilder.command("jar", "cvf", jarName, folderName);
            Process process = processBuilder.start();

            System.out.println("Compiled");
        } catch(Exception e) {
            FMLogUtil.getLog("EventCompileController", "compile")
                    .addDesc("Exception while compiling java file to create jar file ")
                    .addException(e)
                    .logError(LOG);
            throw e;
        }
    }
}
