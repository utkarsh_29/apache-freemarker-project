package com.example.demo.controllers;

import com.example.demo.entity.Attribute;
import com.example.demo.util.FMLogUtil;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateExceptionHandler;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/")
public class InstanceFreeMarkerController {

    private final Logger LOG = LoggerFactory.getLogger(InstanceFreeMarkerController.class);

    final static String pathToTemplate = "src/main/resources/templates/";
    final static String templateName = "EventInstance.ftl";
    final static String pathToOutputFile = "/home/utkarshagrawal/Downloads/test/";

    // Instead of capitalizing className and scriptName in template, we do it here
    String instanceName = StringUtils.capitalize("eventInstance");
    String outputFile = instanceName + ".java";
    String conditions;

    String scriptBody = "List cirList = (List) getObjectFromDS();\n" +
            "        if(cirList!=null){\n" +
            "            List<Map> s = (List) ((LinkedHashMap) cirList\n" +
            "                    .get(0)).get(\"creditAccounts\");\n" +
            "\n" +
            "            String str = \"\";\n" +
            "            if(s!=null && s.size()!=0) {\n" +
            "                for (Map m : s) {\n" +
            "                    if (m.get(\"type\").equals(\"CREDIT_CARD\"))\n" +
            "                    {\n" +
            "\t\t\tString b = \"\";\n" +
            "                        String a = (String)m.get(\"accountNumber\") ;\n" +
            "\t\t\tif(a!=null)\n" +
            "{\n" +
            "                       b = a.replace(\"#\",\"a\");\n" +
            "}\n" +
            "                        {str = str + b + \";\";\n" +
            "                        }\n" +
            "                    }   }\n" +
            "            }\n" +
            "            if(str.length()>0){\n" +
            "            \t System.out.println(str.substring(0,str.length()-1));\n" +
            "                return str.substring(0,str.length()-1);\n" +
            "            }\n" +
            "\n" +
            "        }\n" +
            "        return \"\";";

    @GetMapping("eventMarker")
    public void generate() throws Exception {
        try {
            JSONObject condition1 = new JSONObject();
            JSONObject condition2 = new JSONObject();
            JSONArray conditionsToCheck = new JSONArray();

            Attribute attribute1 = new Attribute();
            Attribute attribute2 = new Attribute();
            List<String> imports1 = new ArrayList<>();
            List<String> imports2 = new ArrayList<>();
            List<Attribute> attributeList = new ArrayList<>();

            condition1.put("First Name", "John");
            condition1.put("Last Name", "Doe");
            condition1.put("City", "!Gwalior");

            condition2.put("First Name", "John");
            condition2.put("Last Name", "Doe");
            condition2.put("City", "Gwalior");

            conditionsToCheck.put(condition1);
            conditionsToCheck.put(condition2);

            imports1.add("import com.example.demo.entity.Attribute;");
            imports1.add("import com.example.demo.entity.EventModuleContext;");
            imports2.add("import com.example.demo.util.SourceUtil;");
            imports2.add("import com.example.demo.util.FMLogUtil;");

            attribute1.setPropertyName("userId");
            attribute1.setScriptName(StringUtils.capitalize("userId"));
            attribute1.setScriptBody(scriptBody);
            attribute1.setImports(imports1);

            attribute2.setPropertyName("merchantId");
            attribute2.setScriptName(StringUtils.capitalize("merchantId"));
            attribute2.setScriptBody(scriptBody);
            attribute2.setImports(imports2);

            attributeList.add(attribute1);
            attributeList.add(attribute2);

            conditions = conditionsToCheck.toString();
            conditions = conditions.replace("\"", "\\\"");

            /* ------------------------------------------------------------------------ */
            /* You should do this ONLY ONCE in the whole application life-cycle:        */

            /* Create and adjust the configuration singleton */
            Configuration cfg = new Configuration(Configuration.VERSION_2_3_29);
            cfg.setDirectoryForTemplateLoading(new File(pathToTemplate));
            // Recommended settings for new projects:
            cfg.setDefaultEncoding("UTF-8");
            cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
            cfg.setLogTemplateExceptions(false);
            cfg.setWrapUncheckedExceptions(true);
            cfg.setFallbackOnNullLoopVariable(false);

            /* ------------------------------------------------------------------------ */
            /* You usually do these for MULTIPLE TIMES in the application life-cycle:   */

            /* Create a data-model */
            Map root = new HashMap();
            root.put("instanceName", instanceName);
            root.put("conditions", conditions);
            root.put("attributes", attributeList);

            /* Get the template (uses cache internally) */
            Template temp = cfg.getTemplate(templateName);

            /* Merge data-model with template */
            Writer writer = new FileWriter(new File(pathToOutputFile, outputFile));
            temp.process(root, writer);
            writer.flush();
            writer.close();

            System.out.println("Generated " + outputFile);

        } catch(Exception e) {
            FMLogUtil.getLog("InstanceFreeMarkerController", "generate")
                    .addDesc("Exception while creating java file from freemarker ")
                    .addException(e)
                    .logError(LOG);
            throw e;
        }
    }

}
