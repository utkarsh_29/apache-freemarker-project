package com.example.demo.controllers;

public class RiskInstanceBaseResponseDTO {

    private int instanceId;

    private String instanceName;

    private String triggerCondition;

    private String prePropertyScript;

    private String postPropertyScript;

    private String riskDataService;

    private String status;

    private String description;

    private int windowId;

    private String windowName;

    private int cloneOf;

    private String modifiedBy;

    private String modifiedAt;

    public int getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(int instanceId) {
        this.instanceId = instanceId;
    }

    public String getInstanceName() {
        return instanceName;
    }

    public void setInstanceName(String instanceName) {
        this.instanceName = instanceName;
    }

    public String getTriggerCondition() {
        return triggerCondition;
    }

    public void setTriggerCondition(String triggerCondition) {
        this.triggerCondition = triggerCondition;
    }

    public String getPrePropertyScript() {
        return prePropertyScript;
    }

    public void setPrePropertyScript(String prePropertyScript) {
        this.prePropertyScript = prePropertyScript;
    }

    public String getPostPropertyScript() {
        return postPropertyScript;
    }

    public void setPostPropertyScript(String postPropertyScript) {
        this.postPropertyScript = postPropertyScript;
    }

    public String getRiskDataService() {
        return riskDataService;
    }

    public void setRiskDataService(String riskDataService) {
        this.riskDataService = riskDataService;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getWindowId() {
        return windowId;
    }

    public void setWindowId(int windowId) {
        this.windowId = windowId;
    }

    public String getWindowName() {
        return windowName;
    }

    public void setWindowName(String windowName) {
        this.windowName = windowName;
    }

    public int getCloneOf() {
        return cloneOf;
    }

    public void setCloneOf(int cloneOf) {
        this.cloneOf = cloneOf;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public String getModifiedAt() {
        return modifiedAt;
    }

    public void setModifiedAt(String modifiedAt) {
        this.modifiedAt = modifiedAt;
    }
}
