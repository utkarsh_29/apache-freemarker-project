package test;

import java.util.*;
import java.lang.*;
import org.json.JSONObject;
import org.json.JSONArray;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.example.demo.entity.Attribute;

<#list attributes as attribute>
    <#list attribute.imports as import>
        ${import}
    </#list>
</#list>

public class ${instanceName} {

    private final Logger LOG = LoggerFactory.getLogger(${instanceName}.class);
    final char NOT_EQUAL = '!';

    public String getInstanceName() {
        return "${instanceName}";
    }

    public boolean isCall(EventModuleContext emContext) {

        try {
            System.out.println("Inside isCall");

            Map<String, String> extendDataMap = emContext.getExtendDataMap();
            List<Map<String, String>> routingConditionList = null;
            String conditions = "${conditions}";

            System.out.println("isCall " + emContext.getExtendDataMap());

            if(conditions.isEmpty()){
                return false;
            }

            ObjectMapper mapper = new ObjectMapper();

            routingConditionList = mapper.readValue(conditions, new TypeReference<List<Map<String, String>>>(){});

            System.out.println(routingConditionList);

             for(int i = 0; i < routingConditionList.size(); i++) {
                if (checkIfAllSubConditionsMatched(routingConditionList.get(i), extendDataMap))
                {
                    System.out.println("True");
                    return true;
                }
             }
        } catch (Exception e){
            FMLogUtil.getLog("${instanceName}", "isCall")
                    .addDesc("Exception while calling isCall ")
                    .addException(e)
                    .logError(LOG);
        }
        return false;
    }

    private boolean checkIfAllSubConditionsMatched(Map<String, String> routingCondition, Map <String,String> extendDataMap)
    {
        try {
            System.out.println("Inside Conditions");

            for (Map.Entry<String,String> entry : routingCondition.entrySet()) {
                if(!checkIfSubConditionMatched(entry.getKey(), entry.getValue(), extendDataMap)) {
                    return false;
                }
            }
        } catch(Exception e) {
            FMLogUtil.getLog("${instanceName}", "checkIfAllSubConditionsMatched")
                    .addDesc("Exception while calling checkIfAllSubConditionsMatched ")
                    .addException(e)
                    .logError(LOG);
        }
        return true;
    }

    private boolean checkIfSubConditionMatched(String key, String value, Map<String, String> extendDataMap) {

        try {
            if(value.charAt(0) == NOT_EQUAL) {
                if(value.substring(1).equals(extendDataMap.get(key))) {
                    return false;
                }
            } else if(!value.equals(extendDataMap.get(key))) {
                return false;
            }
        } catch(Exception e) {
            FMLogUtil.getLog("${instanceName}", "checkIfSubConditionMatched")
                    .addDesc("Exception while calling checkIfSubConditionMatched ")
                    .addException(e)
                    .logError(LOG);
        }
        return true;
    }

    public void preExecute(EventModuleContext emContext) {

        try {
            System.out.println("Inside preExecute() " + emContext.getExtendDataMap());

            <#list attributes as attribute>
                emContext.addStdProperties("${attribute.propertyName}", get${attribute.scriptName}(emContext));
            </#list>

            System.out.println(emContext.getContext());
        } catch(Exception e) {
            FMLogUtil.getLog("${instanceName}", "preExecute")
                    .addDesc("Exception while calling preExecute function ")
                    .addException(e)
                    .logError(LOG);
        }


    }

    public void postExecute(EventModuleContext emContext) {

        try {
            System.out.println("Inside postExecute() " + emContext.getExtendDataMap());

            <#list attributes as attribute>
                emContext.addStdProperties("${attribute.propertyName}", get${attribute.scriptName}(emContext));
            </#list>

            System.out.println(emContext.getContext());
        } catch(Exception e) {
           FMLogUtil.getLog("${instanceName}", "postExecute")
                   .addDesc("Exception while calling postExecute function ")
                   .addException(e)
                   .logError(LOG);
        }

    }

    <#list attributes as attribute>
        private String get${attribute.scriptName}(EventModuleContext emContext) {
            try {
                System.out.println("Inside get" + "${attribute.scriptName}");
                ${attribute.scriptBody}
            } catch(Exception e) {
                FMLogUtil.getLog("${instanceName}", "get" + "${attribute.scriptName}")
                        .addDesc("Exception while calling function " + "get" + "${attribute.scriptName}")
                        .addException(e)
                        .logError(LOG);
            }
            return null;
        }
    </#list>

    private Object getObjectFromDS() {

        Map<String, String> m = new HashMap<>();
        List<Map> s = new ArrayList<>();
        LinkedHashMap<String, List<Map>> linkedHashMap = new LinkedHashMap<>();
        List<LinkedHashMap<String, List<Map>>> cirList = new ArrayList<>();

        m.put("type", "CREDIT_CARD");
        m.put("accountNumber", "123##abc");
        s.add(m);
        linkedHashMap.put("creditAccounts", s);
        cirList.add(linkedHashMap);
        System.out.println(cirList);

        return cirList;
    }


}